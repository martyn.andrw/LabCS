let now
let start
let time = 20 * 60 // sec
let isStarted = 0

function startTimer() {
    if (isStarted == 0) {
        start = new Date()
        isStarted = true

        document.getElementById('checkbox1').hidden = false
        document.getElementById('checkbox2').hidden = false
        document.getElementById('checkbox3').hidden = false
        document.getElementById('checkbox4').hidden = false
        document.getElementById('checkbox5').hidden = false
        document.getElementById('checkbox6').hidden = false

        document.getElementById("starttimer").innerText = "Завершить выполнение"
        var t = setTimeout(function(){ processTimer() }, 1000)
    } else if (isStarted == 1) {
        isStarted = 2
        endTimer()
    }
}

function processTimer() {
    if (isStarted == 1) {
        let smin = start.getMinutes()
        let ssec = start.getSeconds()
        let shour = start.getHours()

        now = new Date()
        let nmin = now.getMinutes()
        let nsec = now.getSeconds()
        let nhour = now.getHours()
        if (nhour < shour) { nhour += 24 }

        let lsec = ((nhour - shour) * 60 + (nmin - smin)) * 60 + (nsec - ssec)
        let lmin = 0

        if (lsec >= time) {
            document.getElementById("timer").innerText = "Время вышло!"
            isStarted = 2
            endTimer()
        } else {
            lsec = time - lsec
            lmin = ~~(lsec / 60)
            lsec = lsec - lmin * 60

            document.getElementById("timer").innerText = convertTimer(lmin, lsec)
            var t = setTimeout(function(){ processTimer() }, 1000)
        }
    } else if (isStarted == 2) {
        endTimer()
    }
}

function endTimer() {
    checkCheckbox()
    document.getElementById("timer").innerText = "Завершен"
}

function convertTimer(cmin, csec) {
    strmin = String(cmin)
    strsec = String(csec)
    if (strmin.length < 2 && strmin.length > 0) {
        strmin = "0" + strmin
    }
    if (strsec.length < 2 && strsec.length > 0) {
        strsec = "0" + strsec
    }
    return "Осталось: " + strmin + " минут " + strsec + " секунд."
}

function startPage() {
    ls = time
    lm = ~~(ls / 60)
    ls = ls - lm * 60
    
    document.getElementById('checkbox1').hidden = true
    document.getElementById('checkbox2').hidden = true
    document.getElementById('checkbox3').hidden = true
    document.getElementById('checkbox4').hidden = true
    document.getElementById('checkbox5').hidden = true
    document.getElementById('checkbox6').hidden = true

    document.getElementById("timer").innerText = "Время: " + lm + " мин " + ls + " сек."
}


function checkCheckbox() {
    let test = []
    let canswers = [[1, 2], [1], [1], [-1], [1], [2]]
    let wanswers = [[-1], [2, 3], [2, 3], [1, 2, 3], [2, 3], [1, 3]]

    // test.push(
    //         document.querySelector('#test1CheckBox1').checked || 
    //         document.querySelector('#test1CheckBox2').checked
    //     ); 

    for (indTest = 0; indTest < 6; indTest++) {
        let corAns = true
        for (i = 0; i < canswers[indTest].length; i++) {
            canswers[indTest][i] > 0 ? 
                corAns = corAns && document.querySelector('#test'+indTest+'CheckBox'+canswers[indTest][i]).checked 
                : i += canswers[indTest].length
        }
        for (i = 0; i < wanswers[indTest].length; i++) {
            wanswers[indTest][i] > 0 ? 
                corAns = corAns && !document.querySelector('#test'+indTest+'CheckBox'+wanswers[indTest][i]).checked 
                : i += wanswers[indTest].length
        }
        test.push(corAns);
    }

    let count = 0
    test.forEach(v => {
        console.log(v)
        if (v) { count++ }
    })

    document.getElementById('results').innerText = 'Результат: ' + count + '/' + test.length
}


window.onload = function() {
    startPage()
}