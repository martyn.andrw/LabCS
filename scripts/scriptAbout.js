let disableMenu = false
let changeGif = false
let descriptionOn = false
let data = ""

function setWindow() {
    data = parent.document.URL.substring(parent.document.URL.indexOf('?'), parent.document.URL.length);

    disableMenu = data.substring(0, data.indexOf('&')).includes('disableMenu') && data.substring(0, data.indexOf('&')).includes('true')
    data = data.substring(data.indexOf('&') + 1, data.length)

    changeGif = data.substring(0, data.indexOf('&')).includes('changeGif') && data.substring(0, data.indexOf('&')).includes('true')
    data = data.substring(data.indexOf('&') + 1, data.length)

    descriptionOn = data.includes('descriptionOn') && data.includes('true') && (data.length == 'descriptionOn=true'.length)

    if (disableMenu) {
        document.getElementById('menu').hidden = true
        document.getElementById('buttonBack').hidden = false
    }
    if (changeGif) {
        document.getElementById('gifAbout').src = '../gifs/evarei.gif'
    }
    if (descriptionOn) {
        document.getElementById('postwID').innerText = 'В приложении выполнены первые 6 лабораторных:\n10.1, 13.1, 13.2, 7, 8, 9\n'
    }
}

function backMain() {
    document.location.href = "../index.html";
}

window.onload = function() {
    setWindow()
}