let date
let hour 
let min
let sec
let day
let month
let year

function currentTime() {
    date = new Date()
    hour = date.getHours()
    min = date.getMinutes()
    sec = date.getSeconds()

    if (hour == 0 && sec == 0) {
        currentDate()
    }

    hour = updateStr(hour)
    min = updateStr(min)
    sec = updateStr(sec)

    document.getElementById("clock").innerText = hour + ":" + min + ":" + sec
    var t = setTimeout(function(){ currentTime() }, 1000)
}

function currentDate() {
    date = new Date()
    
    try {
        year = updateStr(date.getFullYear())
        month = updateStr(date.getMonth() + 1)
        day = updateStr(date.getDate())
    } catch (err) {
        console.log(err)

        year = updateStr(2007)
        month = updateStr(1)
        day = updateStr(1)
    }

    document.getElementById("calendar").innerText = day + ":" + month + ":" + year
}

function updateStr(smth) {
    str = String(smth)
    if (str.length < 2 && str.length > 0) {
        str = "0" + str 
    }
    return str
}

function lab101() {
    console.log("hi!")
    window.open("welcome.html", "_blank", "toolbar=no, scrollbars=no, resizable=no, top=200, left=200, width=640, height=740");
}
  
window.onload = function() {
    lab101()
    currentTime()
    currentDate()
}